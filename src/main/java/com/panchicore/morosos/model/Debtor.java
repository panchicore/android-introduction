package com.panchicore.morosos.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by panchicore on 19/02/16.
 */
public class Debtor {
    private String name;
    private String id;
    private Integer amount;
    private String status;

    public Debtor(JSONObject response) throws JSONException {
        setName(response.getString("name"));
        setId(response.getString("id"));
        setAmount(response.getInt("amount"));
        setStatus(response.getString("status"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
