package com.panchicore.morosos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DebtorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debtor);

        setTitle(getIntent().getStringExtra("NAME"));
    }
}
